import os
import json
import cx_Oracle
import pandas as pd
import sqlalchemy
import sys
from sqlalchemy.pool import NullPool

def get_parameters() -> dict:
    """
    Retorna os parametros do arquivo parameters.json
    :return:
    """
    with open(os.path.join(os.path.dirname(os.path.abspath(__file__)), 'pass.json')) as f:
        parameters = json.loads(f.read())
    return parameters


def get_engine_prdme():
    """
    Retorna o objeto com a conexão para o banco de dados
    :return: sqlalchemy.engine.base.Engine
    """
    parametros = get_parameters()
    user_prd = parametros['prdme.db']['user']
    psw_prd = parametros['prdme.db']['pass']
    host_prd = parametros['prdme.db']['host']
    try:
        engine_dw = sqlalchemy.create_engine('oracle+cx_oracle://{}:{}@{}'.format(user_prd, psw_prd, host_prd),
            echo=False, poolclass=NullPool)
    except Exception as e:
        print(e)
        sys.exit()
    return engine_dw


def get_engine_dw():
    parametros = get_parameters()
    user_dw = parametros['dw.db']['user']
    psw_dw = parametros['dw.db']['pass']
    host_dw = parametros['dw.db']['host']
    try:
        engine_dw = sqlalchemy.create_engine(
            'mysql+pymysql://{}:{}@{}:3306/db_dw'.format(user_dw, psw_dw, host_dw),
            echo=False, poolclass=NullPool)
    except Exception as e:
        print(e)
        sys.exit()
    return engine_dw


def get_engine_prd():
    parametros = get_parameters()
    user_dw = parametros['prdme.db']['user']
    psw_dw = parametros['prdme.db']['pass']
    host_dw = parametros['prdme.db']['host']
    try:
        engine_prd = sqlalchemy.create_engine('oracle+cx_oracle://{}:{}@{}'.format(user_dw, psw_dw, host_dw),
            echo=False, poolclass=NullPool,connect_args={'connect_timeout': 1000})
    except Exception as e:
        print(e)
        sys.exit()
    return engine_prd
