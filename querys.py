sql = '''
SELECT E.DS_EMPRESA                    AS NOME_EMPRESA,
       C.CD_CONTRATO                   AS NUMERO_CONTRATO,
       
     
       CASE C.TP_CONTRATO
           WHEN 'A' THEN 'COLETIVO POR ADESÃO'
           WHEN 'E' THEN 'EMPRESA'
           WHEN 'I' THEN 'INDIVIDUAL'
           WHEN 'F' THEN 'FAMILIAR'
           WHEN 'P' THEN 'PRESTADOR'
           WHEN 'U' THEN 'UNIMED'
           ELSE 'NÃO RASTREAVEL' END   AS TP_CONTRATO,
       C.DT_CADASTRO                   AS DATA_CONTRATO,
       (SELECT MAX(D.DT_DESLIGAMENTO)
        FROM DBAPS.DESLIGA_CONTRATO D
        WHERE D.CD_CONTRATO = C.CD_CONTRATO
          AND D.DT_REATIVACAO IS NULL) AS DATA_CANCELAMENTO,
       B.VIDAS_ATUAL,
       B.VIDA_ANIVERSARIO,
       B.VIDA_INICIAL,
       c.DS_IDENT_LOTACAO

FROM (
         SELECT *
         FROM (
                  ------
                  SELECT C.CD_CONTRATO,
                         C.CD_EMPRESA,
                         (
                             SELECT COUNT(DISTINCT UU.CD_MATRICULA)
                             FROM DBAPS.CONTRATO CC
                                      INNER JOIN DBAPS.USUARIO UU ON CC.CD_CONTRATO = UU.CD_CONTRATO
                             WHERE CC.CD_CONTRATO = C.CD_CONTRATO
                               AND UU.SN_ATIVO = 'S'
                         ) AS VIDAS_ATUAL,
                         (
                             SELECT COUNT(DISTINCT UU.CD_MATRICULA)
                             FROM DBAPS.CONTRATO CC
                                      INNER JOIN DBAPS.USUARIO UU ON CC.CD_CONTRATO = UU.CD_CONTRATO
                             WHERE CC.CD_CONTRATO = C.CD_CONTRATO
                               AND CC.DT_CADASTRO <= DBAPS.FNC_ULTIMO_ANIVERSARIO(CC.DT_CADASTRO)
                               AND NOT EXISTS(SELECT 1
                                              FROM DBAPS.DESLIGAMENTO D
                                              WHERE D.CD_MATRICULA = UU.CD_MATRICULA
                                                AND D.DT_REATIVACAO IS NULL
                                                AND TRUNC(D.DT_DESLIGAMENTO) <=
                                                    DBAPS.FNC_ULTIMO_ANIVERSARIO(CC.DT_CADASTRO))
                         ) AS VIDA_ANIVERSARIO,
                         (
                             SELECT COUNT(DISTINCT UU.CD_MATRICULA)
                             FROM DBAPS.CONTRATO CC
                                      INNER JOIN DBAPS.USUARIO UU ON CC.CD_CONTRATO = UU.CD_CONTRATO
                             WHERE CC.CD_CONTRATO = C.CD_CONTRATO
                               AND CC.DT_CADASTRO <= CC.DT_CADASTRO
                               
                               AND NOT EXISTS(SELECT 1
                                              FROM DBAPS.DESLIGAMENTO D
                                              WHERE D.CD_MATRICULA = UU.CD_MATRICULA
                                                AND D.DT_REATIVACAO IS NULL
                                                AND TRUNC(D.DT_DESLIGAMENTO) <= CC.DT_CADASTRO)
                         ) AS VIDA_INICIAL
                  FROM CONTRATO C
                           

                  WHERE C.TP_CONTRATO <> 'I' --and C.CD_CONTRATO = 76366
                  -----
              ) A
     ) B
         INNER JOIN DBAPS.CONTRATO C ON C.CD_CONTRATO = B.CD_CONTRATO AND C.CD_EMPRESA = B.CD_EMPRESA
         LEFT JOIN DBAPS.EMPRESA E ON C.CD_EMPRESA = E.CD_EMPRESA
WHERE VIDA_ANIVERSARIO > 0 and VIDA_ANIVERSARIO <= 50

'''



sql_calendario = '''select * from CALENDARIO_BI C
WHERE 
 trunc(C.PRIMEIRO_DIA_MES) >= TO_DATE('01/01/2021','DD/MM/YY') 
 and 
 trunc(C.PRIMEIRO_DIA_MES) <= TO_DATE('28/02/2022','DD/MM/YY') 
 
 '''


sql_receita = '''
SELECT 
       NVL(MC.VL_PAGO,0) AS RECEITA     

FROM DBAPS.CONTRATO C
INNER JOIN DBAPS.MENS_CONTRATO MC ON MC.CD_CONTRATO = C.CD_CONTRATO
LEFT JOIN DBAPS.EMPRESA E ON C.CD_EMPRESA = E.CD_EMPRESA
WHERE MC.NR_ANO || MC.NR_MES = SUBSTR(('{1}') ,4,7) || SUBSTR(('{1}'),0,2)

AND C.CD_CONTRATO = {0}
'''


sql_despesa = '''

SELECT nvl(SUM(A.VL_TOTAL_PAGO),0) AS "DESPESA"
FROM DBAPS.V_CTAS_MEDICAS A
         LEFT JOIN DBAPS.PRESTADOR B ON A.CD_PRESTADOR_PRINCIPAL = B.CD_PRESTADOR
         LEFT JOIN DBAPS.GUIA C ON A.NR_GUIA = C.NR_GUIA
         LEFT JOIN DBAPS.PRESTADOR_ENDERECO D ON C.CD_PRESTADOR_ENDERECO = D.CD_PRESTADOR_ENDERECO
         LEFT JOIN DBAPS.ESPECIALIDADE E ON C.CD_ESPECIALIDADE = E.CD_ESPECIALIDADE
         LEFT JOIN DBAPS.PROCEDIMENTO F ON A.CD_PROCEDIMENTO = F.CD_PROCEDIMENTO
         INNER JOIN DBAPS.REPASSE_PRESTADOR RP ON RP.CD_REPASSE_PRESTADOR = A.CD_REPASSE_PRESTADOR
         LEFT JOIN DBAPS.PRESTADOR P ON P.CD_PRESTADOR = C.CD_PRESTADOR_EXECUTOR
    AND RP.CD_PRESTADOR = A.CD_PRESTADOR_PAGAMENTO
         INNER JOIN DBAPS.PAGAMENTO_PRESTADOR PP ON RP.CD_PAGAMENTO_PRESTADOR = PP.CD_PAGAMENTO_PRESTADOR
         INNER JOIN DBAPS.USUARIO U ON U.CD_MATRICULA = A.CD_MATRICULA
WHERE 
   A.TP_SITUACAO_CONTA IN ('AA', 'AT')
  AND A.TP_SITUACAO_ITCONTA IN ('AA', 'AT')
  AND A.TP_SITUACAO_EQUIPE IN ('AA', 'AT')
  AND A.TP_ORIGEM = '2'
  AND RP.CD_CON_PAG IS NOT NULL
  AND A.DT_COMPETENCIA = SUBSTR(('{1}'), 4, 7) || SUBSTR(('{1}'), 0, 2)
  AND A.CD_CONTRATO = {0}
  AND A.VL_TOTAL_PAGO > 0
GROUP BY A.DT_COMPETENCIA
'''


sql_copart = '''
select nvl(sum("vl_copart"),0) as valor_copart
from (
                  SELECT

                         SUM(V.vl_total_franquia) AS "vl_copart"

                  FROM DBAPS.V_CTAS_MEDICAS V
                  WHERE NVL(V.vl_total_pago, 0) <> 0
                    AND V.TP_SITUACAO <> 'NA'
                    AND V.vl_total_franquia <> 0
                    AND V.DT_COMPETENCIA = substr('{1}', 4,4)||substr('{1}',1,2)
                  and v.CD_CONTRATO = {0}

                  UNION ALL
                  -- FATOR SINISTRO INTERNACAO
                  SELECT

                         sum(H.VL_FRANQUIA)   AS vl_copart

                  FROM DBAPS.CONTA_HOSPITALAR H
                           INNER JOIN DBAPS.LOTE L ON L.CD_LOTE = H.CD_LOTE
                           INNER JOIN DBAPS.FATURA F ON F.CD_FATURA = L.CD_FATURA
                           INNER JOIN DBAPS.USUARIO U ON U.CD_MATRICULA = H.CD_USUARIO
                  WHERE F.NR_ANO || F.NR_MES = substr('{1}', 4,4)||substr('{1}',1,2)
                       and h.CD_CONTRATO = {0}
                    AND H.CD_MENS_CONTRATO <> 1
                    AND NVL(H.VL_FRANQUIA, 0) <> 0

              ) x
'''