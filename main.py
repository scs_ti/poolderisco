from functions import *
from querys import *
from sqlalchemy import types
import sqlalchemy as sa
from datetime import datetime

# Parametro de conexão com o dw.
parametros = get_parameters()

# Parametro de conexão com o banco de produção
user_prdme = parametros['prdme.db']['user']
psw_prdme = parametros['prdme.db']['pass']
host_prdme = parametros['prdme.db']['host']

# Engine do banco DW
engine_prd = get_engine_prdme()

# 1 - Buscar informaçoes do calendário para o paramentro
with cx_Oracle.connect('{}/{}@{}'.format(user_prdme, psw_prdme, host_prdme)) as connection:
    df_calendar = pd.read_sql(sql_calendario, connection)

df_calendar = df_calendar.sort_values(by=['ID'], ascending=True)

# 2 - Ler a base de contratos
with cx_Oracle.connect('{}/{}@{}'.format(user_prdme, psw_prdme, host_prdme)) as connection:
    df_base_contratos = pd.read_sql(sql, connection)

df_res = pd.DataFrame()

for i, r in df_base_contratos.iterrows():
    print('contrato: ', r[1])

    for idx, row in df_calendar.iterrows():
        print('data: ', row[3])

        with cx_Oracle.connect('{}/{}@{}'.format(user_prdme, psw_prdme, host_prdme)) as connection:
            df_receita = pd.read_sql(sql_receita.format(r[1], row[3]), connection)
            df_despesa = pd.read_sql(sql_despesa.format(r[1], row[3]), connection)
            df_copart = pd.read_sql(sql_copart.format(r[1], row[3]), connection)



        if df_receita.empty:
            receita = 0
        else:
            receita = df_receita.iloc[0][0]

        if df_despesa.empty:
            despesa = 0
        else:
            despesa = df_despesa.iloc[0][0]

        if df_copart.empty:
            copart = 0
        else:
            copart = df_copart.iloc[0][0]


        d = [{'NOME_DA_EMPRESA': df_base_contratos.iloc[i][0],
              'NUMERO_DO_CONTRATO': df_base_contratos.iloc[i][1],
              'TIPO_DO_CONTRATO': df_base_contratos.iloc[i][2],
              'LOTACAO':df_base_contratos.iloc[i][8],
              'DATA_CONTRATO': df_base_contratos.iloc[i][3],
              'DATA_CANCELAMENTO': df_base_contratos.iloc[i][4],
              'BENEFICIARIO_ATIVOS_ANIV': df_base_contratos.iloc[i][6],
              'VL_MENSALIDADE': receita,
              'VL_DESP_ASSISTENCIAL': despesa,
              'VL_COPART':copart,
              'MES_ANO': row[3]}
             ]
        df_res = df_res.append(pd.DataFrame(data=d))


df_res.to_excel('base_pool de risco gerada.xlsx')